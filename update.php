<?php


$path = dirname(__FILE__).DIRECTORY_SEPARATOR.'sys/settings/config.php';
include ($path);

if (isset($set)) {

    if (isset($set['db'])) {
        $set['__db__'] = $set['db'];
    }


    $set['system_modules'] = array (
        0 => '__sys__',
        1 => '__seo__',
        2 => '__secure__',
        3 => '__rss__',
        4 => '__sitemap__',
        5 => '__preview__',
        6 => '__watermark__',
        7 => '__db__',
    );

    $set['installed_modules'] = array (
        0 => 'news',
        1 => 'stat',
        2 => 'loads',
        3 => 'forum',
        4 => 'users',
        5 => 'foto',
        6 => 'chat',
        7 => 'pages',
        8 => 'search',
        9 => 'statistics',
    );

    if ($fopen=@fopen($path, 'w')) {
        $data = '<?php ' . "\n" . 'return ' . var_export($set, true) . "\n" . '?>';
        fputs($fopen, $data);
        fclose($fopen);
    }

}


include_once '/sys/boot.php';


// convert section tables
$Register['DB']->query("ALTER TABLE `news` CHANGE `category_id` `category_id` VARCHAR(255) NOT NULL;");
$Register['DB']->query("ALTER TABLE `stat` CHANGE `category_id` `category_id` VARCHAR(255) NOT NULL;");
$Register['DB']->query("ALTER TABLE `loads` CHANGE `category_id` `category_id` VARCHAR(255) NOT NULL;");
$Register['DB']->query("ALTER TABLE `foto` CHANGE `category_id` `category_id` VARCHAR(255) NOT NULL;");

$Register['DB']->query("ALTER TABLE `comments` ADD `parent_id` INT(11) NULL DEFAULT '0' AFTER `id`;");

$Register['DB']->query("ALTER TABLE `posts` ADD `id_forum` INT(11) NULL DEFAULT '0' AFTER `id_theme`;");
$Register['DB']->query("UPDATE  `posts` SET `id_forum` = (SELECT `id_forum` FROM `themes` WHERE `id` = `posts`.`id_theme`);");

$Register['DB']->query("DROP TABLE IF EXISTS `news_add_fields`;");
$Register['DB']->query("DROP TABLE IF EXISTS `loads_add_fields`;");
$Register['DB']->query("DROP TABLE IF EXISTS `stat_add_fields`;");
$Register['DB']->query("DROP TABLE IF EXISTS `users_add_fields`;");
$Register['DB']->query("DROP TABLE IF EXISTS `news_add_content`;");
$Register['DB']->query("DROP TABLE IF EXISTS `loads_add_content`;");
$Register['DB']->query("DROP TABLE IF EXISTS `stat_add_content`;");
$Register['DB']->query("DROP TABLE IF EXISTS `users_add_content`;");
$Register['DB']->query("CREATE TABLE `add_fields` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `field_id` INT(11) NOT NULL,
    `module` VARCHAR(100) NOT NULL,
    `type` VARCHAR(10) NOT NULL,
    `name` VARCHAR(100) NOT NULL,
    `label` VARCHAR(255) NOT NULL,
    `size` INT(11) NOT NULL,
    `params` VARCHAR(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM CHARACTER SET utf8 COLLATE utf8_general_ci;");

$Register['DB']->query("RENAME TABLE `news_sections` TO `news_categories`");
$Register['DB']->query("RENAME TABLE `foto_sections` TO `foto_categories`");
$Register['DB']->query("RENAME TABLE `loads_sections` TO `loads_categories`");
$Register['DB']->query("RENAME TABLE `stat_sections` TO `stat_categories`");

die(__('Operation is successful'));