<?php
/**
* @project    Atom-M CMS
* @package    Filters
* @url        https://atom-m.net
*/


class Viewer_Filter_Replace {

    private $params = array();

    public function compile($value, Viewer_CompileParser $compiler)
    {
        if (empty($this->params[0])) throw new Exception('First parameter is not exists in "Replace" filter.');
        if (!is_callable($value)) throw new Exception('(Filter_Replace):Value for filtering must be callable.');

        $compiler->raw('strtr(');
        $value($compiler);
        $compiler->raw(', ');
        $this->params[0]->compile($compiler);
        $compiler->raw(')');
    }
    

    public function addParam($param)
    {
        $this->params[] = $param;
    }


    public function __toString()
    {
        $out = '[filter]:replace' . "\n";
        $out .= '[params]:' . implode("<br>\n", $this->params) . "\n";
        return $out;
    }
}