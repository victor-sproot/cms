<?php
/**
* @project    Atom-M CMS
* @package    Filters
* @url        https://atom-m.net
*/


class Viewer_Filter_Urlencode {


    public function compile($value, Viewer_CompileParser $compiler)
    {
        if (!is_callable($value)) throw new Exception('(Filter_Urlencode):Value for filtering must be callable.');

        $compiler->raw('urlencode(');
        $value($compiler);
        $compiler->raw(')');
    }
    
    
    public function __toString()
    {
        $out = '[filter]:urlencode' . "\n";
        return $out;
    }
}