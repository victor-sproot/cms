<?php
/**
* @project    Atom-M CMS
* @package    Filters
* @url        https://atom-m.net
*/


class Viewer_Filter_Bbcode {


    public function compile($value, Viewer_CompileParser $compiler)
    {
        if (!is_callable($value)) throw new Exception('(Filter_Bbcode):Value for filtering must be callable.');

        $compiler->raw('PrintText::print_page(');
        $value($compiler);
        $compiler->raw(')');
    }
    
    
    public function __toString()
    {
        $out = '[filter]:bbcode' . "\n";
        return $out;
    }
}