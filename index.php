<?php
/**
* @project    Atom-M CMS
* @package    Entry dot
* @url        https://atom-m.net
*/

list($usec, $sec) = explode(" ", microtime());

header('Content-Type: text/html; charset=utf-8');


if (file_exists('install')) {
    $set = array();
    if (file_exists('data/config/__main__.php'))
        $set = include_once ('data/config/__main__.php');
    if (!empty($set) && !empty($set['__db__']['name']))
        die('Before use your site, delete INSTALL dir! <br />Перед использованием удалите папку INSTALL');
    
    header('Location: install');
    die();
}


include_once 'sys/boot.php';




/**
 * Parser URL
 * Get params from URL and launch needed module and action
 */
Events::init('before_pather', array());
new AtmUrl($Register);
Events::init('after_pather', array());

?>