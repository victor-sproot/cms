<?php
/**
 * @project     Atom-M CMS
 * @package     Admin Panel module
 * @url         https://atom-m.net
 */


include_once '../sys/boot.php';
include_once ROOT . '/admin/inc/adm_boot.php';




/**
 * Return current module which we editing
 */
function getCurrMod() {
    if (empty($_GET['mod'])) {
        $_SESSION["message"][] = __('Some error occurred');
        redirect('/admin/');
    }
    
    $mod = trim($_GET['mod']);
    if (!Config::read($mod.".std_admin_pages.category")) {
        $_SESSION["message"][] = __('Some error occurred');
        redirect('/admin/');
    }
    return $mod;
}




/**
 * Try find collision
 */
function deleteCatsCollision() 
{
    global $DB;
    $collision = $DB->select(getCurrMod() . '_categories', DB_ALL, array(
        'joins' => array(
            array(
                'type' => 'LEFT',
                'table' => getCurrMod() . '_categories',
                'alias' => 'b',
                'cond' => '`b`.`id` = `a`.`parent_id`',
            ),
        ),
        'fields' => array('COUNT(`b`.`id`) as cnt', '`a`.*'),
        'alias' => 'a',
        'group' => '`a`.`parent_id`',
    ));
    
    if ($collision && is_array($collision) && count($collision)) {
        foreach ($collision as $key => $cat) {
            if (!empty($cat['parent_id']) && empty($cat['cnt'])) {
                $DB->save(getCurrMod() . '_categories',
                array(
                    'parent_id' => 0,
                ), 
                array(
                    'id' => $cat['id']
                ));
            }
        }
    }
}
deleteCatsCollision();





$head = file_get_contents('template/header.php');
$page_title = __(getCurrMod(),false,getCurrMod());
$popups = '';

$Register = Register::getInstance();
$Register['module'] = getCurrMod();

if (!isset($_GET['ac'])) $_GET['ac'] = 'index';
$permis = array('add', 'del', 'index', 'edit', 'off_home', 'on_home');
if (!in_array($_GET['ac'], $permis)) $_GET['ac'] = 'index';

switch($_GET['ac']) {
    case 'index':
        $content = index($page_title);
        break;
    case 'del':
        $content = delete();
        break;
    case 'add':
        $content = add();
        break;
    case 'edit':
        $content = edit();
        break;
    case 'on_home':
        $content = on_home();
        break;
    case 'off_home':
        $content = off_home();
        break;
    default:
        $content = index();
}




$pageTitle = $page_title;
$pageNav = $page_title;
$pageNavr = '';
include_once ROOT . '/admin/template/header.php';
?>



<blockquote>
    <?php echo __('If you delete a category, all the materials in it will be removed') ?>
</blockquote>




<?php

echo $popups;
echo $content;


function getTreeNode($array, $id = false) {
    $out = array();
    foreach ($array as $key => $val) {
        if ($id === false && empty($val['parent_id'])) {
            $out[$val['id']] = array(
                'category' => $val,
                'subcategories' => getTreeNode($array, $val['id']),
            );
            unset($array[$key]);
        } else {
        
            if ($val['parent_id'] == $id) {
                $out[$val['id']] = array(
                    'category' => $val,
                    'subcategories' => getTreeNode($array, $val['id']),
                );
                unset($array[$key]);
            }
        }
    }
    return $out;
}


function buildCatsList($catsTree, $catsList, $indent = '') {
    global $popups;

    //$Register = Register::getInstance();
    $DB = getDB();
    $acl_groups = ACL::get_group_info();
    $out = '';
    
    if (!$catsTree || !is_array($catsTree)) return $out;
    
    foreach ($catsTree as $id => $node) {
        if (!isset($node['category'])) continue;
        
        $cat = $node['category'];
        $no_access = (isset($cat['no_access']) && $cat['no_access'] !== '') ? explode(',', $cat['no_access']) : array();

        
        $catsList = ($catsList && is_array($catsList) && count($catsList)) ? $catsList : array();
        $cat_selector = '<select  name="id_sec">';
        if (empty($cat['parent_id'])) {
            $cat_selector .= '<option value="0" selected="selected">&nbsp;</option>';
        } else {
            $cat_selector .= '<option value="0">&nbsp;</option>';
        }
        foreach ($catsList as $selector_result) {
            if ($selector_result['id'] == $cat['id']) continue;
            if ($cat['parent_id'] == $selector_result['id']) {
                $cat_selector .= '<option value="' . $selector_result['id'] 
                . '" selected="selected">' . $selector_result['title'] . '</option>';
            } else {
                $cat_selector .= '<option value="' . $selector_result['id'] 
                . '">' . $selector_result['title'] . '</option>';
            }
        }
        $cat_selector .= '</select>';
        
        
        
        $out .= '<li class="collection-item">
                    <span class="title truncate col max-s7" title="' . h($cat['title']) . '">' . $indent . h($cat['title']) . '</span>
                    <span>#' . $cat['id'] . '</span>
                    <span><i class="mdi-content-content-copy tiny"></i>' . $cat['cnt'] . '</span>
                    <div class="right-content">';
                    

        

        if (getCurrMod() != 'foto') {
            if ($cat['view_on_home'] == 1) {
                $out .=  '<a class="btn-floating green" title="' . __('Down') . '" href="?ac=off_home&id='.$cat['id'].'&mod='.getCurrMod().'" onClick="return _confirm();"><i class="mdi-action-home small"></i></a>';
            } else {
                $out .=  '<a class="btn-floating grey" title="' . __('Up') . '" href="?ac=on_home&id='.$cat['id'].'&mod='.getCurrMod().'" onClick="return _confirm();"><i class="mdi-action-home small"></i></a>';
            }
        }
            
            
        $out .= '
                <a href="#' . $cat['id'] . '_cat" class="btn-floating modal-trigger" title="' . __('Edit') . '"><i class="mdi-action-settings small"></i></a>
                 <a class="btn-floating red" title="' . __('Delete') . '" href="?ac=del&id=' . $cat['id'] . '&mod='.getCurrMod().'" onClick="return _confirm();"><i class="mdi-action-delete small"></i></a>
                </div>
        </li>';
            
            
            
        $popups .= '<div id="' . $cat['id'] . '_cat" class="modal modal-fixed-footer">
                <form action="category.php?mod=' . getCurrMod() . '&ac=edit&id=' . $cat['id'] . '" method="POST">
                <div class="modal-content">
                    <h4>' . __('Section editing') . '</h4>
                    <div class="input-field">
                        ' . $cat_selector . '
                        <label>' . __('Parent section') . '</label>
                    </div>
                    <div class="input-field">
                        <input id="title' . $cat['id'] . '" type="text" name="title" value="' . h($cat['title']) . '" />
                        <label for="title' . $cat['id'] . '">' . __('Title') . '</label>
                    </div>
                    <div class="input-field">
                        <div>
                            ' . __('Access for') . ':
                        </div>
                        <div><table><tr>';
                        $n = 0;
                        if ($acl_groups && is_array($acl_groups)) {
                            foreach ($acl_groups as $gid => $group) {
                                if (($n % 3) == 0) $popups .= '</tr><tr>';
                                $checked = (in_array($gid, $no_access)) ? '' : ' checked="checked"';
                                $id = md5(rand(0, 99999) . $n);
                                $popups .= '<td><input id="'. $cat['id'] . '-' . $id . '" type="checkbox" name="access[' . $gid . ']" value="' . $id 
                                . '"' . $checked . '  /><label for="'. $cat['id'] . '-' . $id . '">' . h($group['title']) . '</label></td>';
                                $n++;
                            }
                        }
                        $popups .= '</tr></table></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" value="' . __('Save') . '" name="send" class="btn" />
                    <a href="#!" class="modal-action modal-close btn-flat">ОТМЕНИТЬ</a>
                </div>
                </form>
            </div>';
            
        
        if (isset($node['subcategories']) && is_array($node['subcategories']) && count($node['subcategories'])) {
            $out .= buildCatsList($node['subcategories'], $catsList, $indent . '<i class="mdi-hardware-keyboard-tab left small"></i>');
        }
    }
    
    return $out;
}



function index(&$page_title) {
    global $popups;

    //$Register = Register::getInstance();
    $DB = getDB();
    $acl_groups = ACL::get_group_info();


    $page_title .= ' - ' . __('Sections editor');
    $cat_selector = '<select name="id_sec">';
    $cat_selector .= '<option value="0">&nbsp;</option>';
    $all_sections = $DB->select(getCurrMod() . '_categories', DB_ALL, array(
        'joins' => array(
            array(
                'alias' => 'b',
                'type' => 'LEFT',
                'table' => getCurrMod(),
                'cond' => 'a.`id` = b.`category_id`',
            ),
        ),
        'fields' => array('a.*', 'COUNT(b.`id`) as cnt'),
        'alias' => 'a',
        'group' => 'a.`id`',
    ));
    if (!$all_sections || !is_array($all_sections)) $all_sections = array();
    
    foreach ($all_sections as $result) {
        $cat_selector .= '<option value="' . $result['id'] . '">' . h($result['title']) . '</option>';
    }
    $cat_selector .= '</select>';
    
    $html = '';
    
    
    
    $cats_tree = getTreeNode($all_sections);

    
    
    
    $popups .= '<div id="addCat" class="modal modal-fixed-footer">
            <form action="category.php?mod=' . getCurrMod() . '&ac=add" method="POST">
            <div class="modal-content">
                <h4>' . __('Add section') . '</h4>
                <div class="b30tm">
                    <div class="input-field">
                        ' . $cat_selector . '
                        <label>' . __('Parent section') . '</label>
                    </div>
                    <div class="input-field">
                        <input id="title" type="text" name="title" />
                        <label for="title">' . __('Title') . '</label>
                    </div>
                    <div class="input-field">
                        <div>
                            ' . __('Access for') . ':
                        </div>
                        <div>
                            <table class="checkbox-collection"><tr>';
                            $n = 0;
                            if ($acl_groups && is_array($acl_groups)) {
                                foreach ($acl_groups as $gid => $group) {
                                    if (($n % 3) == 0) $popups .= '</tr><tr>';
                                    $id = md5(rand(0, 99999) . $n);
                                    $popups .= '<td><input id="ac_' . $id . '" type="checkbox" name="access[' . $gid . ']" value="' . $id 
                                    . '"  checked="checked" /><label for="ac_' . $id . '">' . h($group['title']) . '</label></td>';
                                    $n++;
                                }
                            }
                            $popups .= '</tr></table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="type" value="cat" />
                <input type="submit" value="' . __('Add') . '" name="send" class="btn" />
                <a href="#!" class="modal-action modal-close btn-flat">ОТМЕНИТЬ</a>
            </div>
            </form>
        </div>';
    
    
    
    
    $html .= '<div class="row">
        <a class="btn modal-trigger right" href="#addCat"><i class="mdi-content-add left"></i>' . __('Add section') . '</a>
        <h4 class="light">' . __('Sections editor') . '</h4>

        <ul class="collection">';
            
            
    if (count($all_sections) > 0) {
        $html .= buildCatsList($cats_tree, $all_sections); 	
    } else {
        $html .= __('Sections not found');
    }
    
    
    $html .= '</ul></div>';

    
    return $html;
}




function edit() {

    if (!isset($_GET['id'])) redirect('/admin/category.php?mod=' . getCurrMod());
    if (!isset($_POST['title'])) redirect('/admin/category.php?mod=' . getCurrMod());
    $id = intval($_GET['id']);
    
    if ($id < 1) redirect('/admin/category.php?mod=' . getCurrMod());
    
    
    global $DB;
    
    $acl_groups = ACL::get_group_info();
    
    
    $error = array();

    if (empty($_POST['title'])) $error[] = __('Empty field "title"');
    


    $parent_id = intval($_POST['id_sec']);
    $changed_cat = $DB->select(getCurrMod() . '_categories', DB_FIRST, array('cond' => array('id' => $id)));
    if (empty($changed_cat)) $error[] = __('Edited section not found');

    
    /* we must know changed parent section or not changed her. And check her  */
    if (!empty($parent_id) && $changed_cat[0]['parent_id'] != $parent_id) {
        $target_section = $DB->select(getCurrMod() . '_categories', DB_COUNT, array('cond' => array('id' => $parent_id)));
        if ($target_section < 1) $error[] = __('Parent section not found');
    }
    /* if errors exists */
    if (!empty($error)) {
        $_SESSION['message'] = array_merge($_SESSION['message'], $error);
        redirect('/admin/category.php?mod=' . getCurrMod());
    }
    
    
    $no_access = array();
    if ($acl_groups && is_array($acl_groups)) {
        foreach ($acl_groups as $gid => $group) {
            if (!array_key_exists($gid, $_POST['access'])) {
                $no_access[] = $gid;
            }
        }
    }
    $no_access = (count($no_access) == 1 && $no_access[0] !== '') ? intval($no_access[0]) : implode(',', $no_access);
    
    
    /* prepare data to save */
    $data = array(
        'id' => $id, 
        'title' => substr($_POST['title'], 0, 100), 
        'no_access' => $no_access,
    );
    if (isset($parent_id)) $data['parent_id'] = (int)$parent_id;
    $DB->save(getCurrMod() . '_categories', $data);
        

    redirect('/admin/category.php?mod=' . getCurrMod());
}



function add() {
    global $DB;
    if (empty($_POST['title'])) redirect('/admin/category.php?mod=' . getCurrMod());
    
    
    
    $acl_groups = ACL::get_group_info();
    
    
    $error = array();
    $title = getDB()->escape($_POST['title']);
    $in_cat = intval($_POST['id_sec']);
    if ($in_cat < 0) $in_cat = 0;
    
    
    if (empty($title)) $error[] = __('Empty field "title"');
    
    $no_access = array();
    if ($acl_groups && is_array($acl_groups)) {
        foreach ($acl_groups as $id => $group) {
            if (!array_key_exists($id, $_POST['access'])) {
                $no_access[] = $id;
            }
        }
    }
    $no_access = (count($no_access)) ? implode(',', $no_access) : '';
    if ($no_access !== '') $no_access = new Expr($no_access);
    
    /* if errors exists */
    if (!empty($error)) {
        $_SESSION['message'] = array_merge($_SESSION['message'], $error);
        redirect('/admin/category.php?mod=' . getCurrMod());
    }
    
    
    if (empty($error)) {
        $DB->save(getCurrMod() . '_categories', array(
            'title' => $title,
            'parent_id' => $in_cat,
            'no_access' => $no_access,
        ));
    }
        
    redirect('/admin/category.php?mod=' . getCurrMod());
}


function delete() {
    global $DB;
    $id = (!empty($_GET['id'])) ? intval($_GET['id']) : 0;
    if ($id < 1) redirect('/admin/category.php?mod=' . getCurrMod());
    
    $childCats = OrmManager::getModelInstance(getCurrMod() . 'Categories')->getOneField('id', array('parent_id' => $id));
    $ids = '`category_id` LIKE \'%'.$id.'%\'';
    if ($childCats && is_array($childCats) && count($childCats) > 0)
        $ids .= ' OR `category_id` IN (' . implode(', ', array_unique($childCats)) . ')';
    $where = array($ids);


    $COUNT = $DB->select(getCurrMod(), DB_COUNT, array('cond' => $where));
    if ($COUNT > 0) {
        $_SESSION['message'][] = __('Category is not empty.');
        redirect('/admin/category.php?mod=' . getCurrMod());
    }
    
    $childrens = $DB->select(getCurrMod() . '_categories', DB_ALL, array('cond' => array('parent_id' => $id)));
    
    if (!$childrens || !is_array($childrens) || !count($childrens)) {
        delete_category($id);
    } else {
        foreach ($childrens as $category) {
            delete_category($category['id']);
            delete($category['id']);
        }
        $DB->query("DELETE FROM `" . $DB->getFullTableName(getCurrMod() . '_categories') . "` WHERE `id`='{$id}'");
    }
    redirect('/admin/category.php?mod=' . getCurrMod());
}


function delete_category($id) {
    global $DB;
    $records = $DB->select(getCurrMod(), DB_ALL, array('cond' => array('category_id' => $id)));
    if ($records || is_array($records) || count($records) > 0) {
        $atmurl = new AtmUrl;
        foreach ($records as $record) {
            $DB->query("DELETE FROM `" . $DB->getFullTableName(getCurrMod()) . "` WHERE `id`='{$record['id']}'");
            
            
            $hlufile = $atmurl->searchHluById($record['id'], getCurrMod());
            if (file_exists($hlufile))
                _unlink($hlufile);
            
            
            
            if (getCurrMod() == 'foto') {
                if (file_exists(ROOT . '/data/files/foto/full/' . $record['filename'])) 
                    _unlink(ROOT . '/data/files/foto/full/' . $record['filename']);
                if (file_exists(ROOT . '/data/files/foto/preview/' . $record['filename'])) 
                    _unlink(ROOT . '/data/files/foto/preview/' . $record['filename']);

                    
            } else {
                $attaches = $DB->select(getCurrMod() . '_attaches', DB_ALL, array('cond' => array('entity_id' => $record['id'])));
                if ($attaches && is_array($attaches) && count($attaches)) {
                    foreach ($attaches as $attach) {
                        $DB->query("DELETE FROM `" . $DB->getFullTableName(getCurrMod() . '_attaches') 
                        . "` WHERE `id`='{$attach['id']}'");
                        if (file_exists(ROOT . '/data/files/' . getCurrMod() . '/' . $attach['filename']))
                            _unlink(ROOT . '/data/files/' . getCurrMod() . '/' . $attach['filename']);
                    }
                }
                
                if (getCurrMod() == 'loads') {
                    if (file_exists(ROOT . '/data/files/loads/' . $record['download'])) 
                        _unlink(ROOT . '/data/files/loads/' . $record['download']);
                }
            } 
        }
    }
    $DB->query("DELETE FROM `" . $DB->getFullTableName(getCurrMod() . '_categories') . "` WHERE `id`='{$id}'");
    return true;
}



function on_home($cid = false) {
    global $DB;
    if (getCurrMod() == 'foto') redirect('/admin/category.php?mod=' . getCurrMod());
    
    
    if ($cid === false) {
        $id = (!empty($_GET['id'])) ? intval($_GET['id']) : 0;
        if ($id < 1) redirect('/admin/category.php?mod=' . getCurrMod());
    } else {
        $id = $cid;
    }

    
    $childs = $DB->select(getCurrMod() . '_categories', DB_ALL, array('cond' => array('parent_id' => $id)));
    if ($childs && is_array($childs) && count($childs)) {
        foreach ($childs as $child) {
            on_home($child['id']);
        }
    } 
    
    $DB->save(getCurrMod() . '_categories', array('id' => $id, 'view_on_home' => 1));
    $DB->save(getCurrMod(), array('view_on_home' => 1), array('category_id' => $id));

        
    if ($cid === false) redirect('/admin/category.php?mod=' . getCurrMod());
}



function off_home($cid = false) {
    global $DB;
    if (getCurrMod() == 'foto') redirect('/admin/category.php?mod=' . getCurrMod());
    
    
    if ($cid === false) {
        $id = (!empty($_GET['id'])) ? intval($_GET['id']) : 0;
        if ($id < 1) redirect('/admin/category.php?mod=' . getCurrMod());
    } else {
        $id = $cid;
    }

    
    $childs = $DB->select(getCurrMod() . '_categories', DB_ALL, array('cond' => array('parent_id' => $id)));
    if ($childs && is_array($childs) && count($childs)) {
        foreach ($childs as $child) {
            off_home($child['id']);
        }
    } 
    
    $DB->save(getCurrMod() . '_categories', array('id' => $id, 'view_on_home' => 0));
    $DB->save(getCurrMod(), array('view_on_home' => 0), array('category_id' => $id));

        
    if ($cid === false) redirect('/admin/category.php?mod=' . getCurrMod());
}


?>

<script>
$(document).ready(function(){
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    $('.modal-trigger').leanModal();
    $('select').material_select();
});
</script>



<?php
include_once 'template/footer.php';
