<?php

$output    = '';
$template_patch = dirname(__FILE__).'/template/users.html';
$conf_pach = dirname(__FILE__).'/config.json';
$config = json_decode(file_get_contents($conf_pach), true);

if (isset($_POST['send'])) {
  
    $config['limit'] = $_POST['limit'];
	$config['view_banned'] = $_POST['view_banned'];
	
	$config['usersort'] = $_POST['sortUser'];
	file_put_contents($conf_pach, json_encode($config));
    $f = fopen($template_patch, "w");
    fwrite($f,$_POST['template']);
    
    $Cache = new Cache;
    $Cache->remove('pl_users_rating');
  
    $output .= '<div class="warning">Изменения приняты!</div>';
}

if ($config['usersort']=="posts") {
    $sort="сообщениям на форуме";
} else if ($config['usersort']=="rating"){
    $sort="репутации";
} else { 
    $sort="дате регистрации";
}

$template = file_get_contents($template_patch);

$output .= '<style>
                .ib {
                    font-weight: bold;
                    font-style: italic;
                }
                .right {
                    width: 50%;
                }
                input[type="text"] {
                    height: auto;
                }
				.markers input {
                    background: #D9D9D9;
                    border-radius: 5px;
                    display: inline-block;
                    font-weight: 700;
                    padding: 5px;border:none;width:130px;text-align:center;
                }
                .mmand {
                    color:#fff;
                    font-weight:bold;
                    text-align:center;
                    padding-top:4px;
                }
            </style>';

$output .= '<form action="" method="post">
                <div class="list">
                    <div class="title">Управление плагином Топ пользователей</div>
                    <div class="level1">

                            <div class="items">
                                <div class="setting-item">
                                    <div class="title" style="padding: 20px; text-align:left">Метка для вывода топа пользователей {{ users_rating }}</div>
                                </div>
                            </div>

                            <div class="items">
                                <div class="setting-item">
                                    <div class="left">Сколько человек выводить в топе:</div>
                                    <div class="right">
                                        <input type="text" size="100" name="limit" value="' . $config['limit'] . '">
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
						
							 <div class="items">
                                <div class="setting-item">
                                    <div class="left">Cпособ сортировки:
									<div style="font-size:10px">Сортировка идет по '.$sort.'</div>
									</div>
                                    <div class="right">
									
                                        <select name="sortUser">
										<option value="rating">По репутации</option>
										<option value="posts">По количеству сообщений на форуме</option>
										<option value="puttime">По дате регистрации</option>
										</select>
										 
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>

							 <div class="items">
                                <div class="setting-item">
                                    <div class="left">Показывать забаненых пользователей
									
									</div>
                                    <div class="right">
									
                                        <select name="view_banned">
										<option value="1">Да</option>
										<option value="0">Нет</option>
										
										</select>
										 
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>

                            <div class="items">
                                <div class="setting-item">
                                    <textarea name="template" style="width: 99%; height: 350px">' . (isset($template) ? htmlspecialchars($template) : '') . '</textarea>
                                </div>
                            </div>

                            <div class="items">
                                <div class="setting-item">
                                    <div class="title" style="padding: 20px; text-align:center; height: auto;"><input name="send" type="submit" value="Сохранить" class="save-button"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>';

$output .= '<ul class="markers">
<div style="color:#ff0000">В плагине работают все поля которые есть в базе данных users. <br/>
Пример: {{user.названиеполя }}. <br /><b>Для новичков</b> какие поля есть: id; name; full_name; state; rating; url; icq; cite; byear(Дата рождения);last_visit; themes; posts;
</div>
                <li><input onclick="select(this)" value="{{ user.avatar }}" readonly="" /> - ссылка на аватар пользователя, если нету выводит noavatar</li>
                <li><input onclick="select(this)" value="{{ user.profile_url }}" readonly="" /> - ссылка на профиль пользователя</li>
				<li><input onclick="select(this)" value="{{ user.name }}" readonly="" /> - Логин пользователя</li>
                <li><input onclick="select(this)" value="{{ user.posts }}" readonly="" /> - Количество постов на форуме</li>
                <li><input onclick="select(this)" value="{{ user.rating }}" readonly="" /> - Количество репутации </li>
				<li><input onclick="select(this)" value="{{ user.load }}" readonly="" /> - Количество файлов </li>
				<li><input onclick="select(this)" value="{{ user.news }}" readonly="" /> - Количество новостей </li>
				<li><input onclick="select(this)" value="{{ user.publ }}" readonly="" /> - Количество статей</li>
				<li><input onclick="select(this)" value="{{ user.comments }}" readonly="" /> - Количество комментариев </li>
            </ul>
			<div class="mmand">Версия 0.9 :) Специально для Fapos.net</div>
			';

?>