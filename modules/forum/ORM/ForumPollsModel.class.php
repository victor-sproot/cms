<?php
/**
* @project    Atom-M CMS
* @package    Polls Model
* @url        https://atom-m.net
*/


namespace ForumModule\ORM;

class ForumPollsModel extends \OrmModel
{
    public $Table = 'polls';

    protected $RelatedEntities;

}