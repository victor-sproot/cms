<?php
/**
* @project    Atom-M CMS
* @package    UsersVotes Model
* @url        https://atom-m.net
*/


namespace UsersModule\ORM;

class UsersVotesModel extends \OrmModel
{
    public $Table  = 'users_votes';

    protected $RelatedEntities = array(
        'touser' => array(
            'model' => 'Users',
            'type' => 'has_one',
            'foreignKey' => 'to_user',
        ),
        'fromuser' => array(
            'model' => 'Users',
            'type' => 'has_one',
            'foreignKey' => 'from_user',
        ),
    );
}