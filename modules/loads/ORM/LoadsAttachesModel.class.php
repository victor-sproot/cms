<?php
/**
* @project    Atom-M CMS
* @package    LoadsAttaches Model
* @url        https://atom-m.net
*/


namespace LoadsModule\ORM;

class LoadsAttachesModel extends \OrmModel
{

    public $Table = 'loads_attaches';



    public function getByEntity($entity)
    {
        $params['entity_id'] = $entity->getId();
        $data = $this->getMapper()->getCollection($params);
        return $data;
    }


}