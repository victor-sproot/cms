<?php
return array(
    'icon_class' => 'mdi-action-assessment',
    'pages'   => array(
        WWW_ROOT.'/admin/settings.php?m=statistics'      => __('Settings'),
        WWW_ROOT.'/admin/statistics/statistic.php'       => __('Watch statistic',false,'statistics'),
    ),
);