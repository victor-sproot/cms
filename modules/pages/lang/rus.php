<?php
return array(

    'pages'                                       => 'Cтраницы ',

    // Pages editor
    'Pages editor'                                => 'Редактор страниц',
    'Editing page'                                => 'Редактирование страницы',
    'View on menu?'                               => 'Видна ли в меню',
    'Dinamic page tag'                            => 'Динамический тег <span class="comment">(для использования в шаблоне)</span>',

);
